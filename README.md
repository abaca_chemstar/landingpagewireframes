# README #



### What is this repository for? ###

* Wireframe repository for viewing and reference
* Each html file corresponds with a specific step or workflow
* Styles are from https://developer.salesforce.com/lightning/design-system using Salesforce's own Lightning Design System
* Additional Grid layout and javascript functionality is from http://getbootstrap.com/ Twitters Bootstrap.
* All necessary files are bundled and local.

### How do I get set up? ###

There are two ways to download respository:
* The first and probably the easiest is in the Portal Wireframes Repository, on the left hand side bar is the navigation section. At the bottom of that section is a downloads Link. Click on that link and there is a link to download repository. This will download the .zip of the entire project. Useful for quick downloads.
* The second method is to git clone using instructions in the clone section. You can clone using Atlassian like the page documents or use any other .git tracking method you choose. If you .git clone and checkout, this will verify you always have the latest version. 

### Note ###
* There is a bug where svg icons are not showing up - this has to do with how svg is handled security-wise in certain browsers.
* If you would like icons to show up, as a workaround for now, open html files in Firefox Developer Edition and view. 

### Use Guidlines guidelines ###
* If new content is added or changed, please create a fork and create a pull request for changes to be reviewed. 

### Who do I talk to? ###

* Wireframes maintained By Adam - Rinchem Company
* contact here: abaca@rinchem.com or create an issue. 